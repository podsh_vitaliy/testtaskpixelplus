<?php
use Notice\Sender;

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementUpdate");

function OnBeforeIBlockElementUpdate(&$arFields)
{
    if ($arFields ["IBLOCK_ID"] == IBLOCK_ID_PRODUCTION && $arFields ["ACTIVE"] == "N") {
        $res = CIBlockElement::GetByID($arFields["ID"]);
        if ($ar_res = $res->GetNext()) {
            $showCounter = intval($ar_res["SHOW_COUNTER"]);
            if ($showCounter > 2) {
                global $APPLICATION;
                $APPLICATION->throwException("Товар невозможно деактивировать, у него " . intval($ar_res["SHOW_COUNTER"]) . " просмотров");
                return false;
            }

        }

    }
}


AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdate");
AddEventHandler("main", "OnBeforeUserAdd", "OnBeforeUserAdd");
AddEventHandler("main", "OnBeforeGroupUpdate", "OnBeforeGroupUpdate");

function OnBeforeGroupUpdate($id, &$arFields){
    if ($id == USER_GROUP_ID_CONTENT_EDITORS) {

        $usersInGroup = CGroup::GetGroupUser($id);
        $usersNotice = array();

        foreach ($arFields["USER_ID"] as $user) {
            if (!in_array($user["USER_ID"], $usersInGroup)) {
                continue;
            }
            $usersNotice[] = $user["USER_ID"];
        }
        if (!empty($usersNotice)) {
            Sender::noticeGroupOnAddUser($usersNotice);
        }
    }
}

function OnBeforeUserAdd(&$arFields){
    foreach ($arFields["GROUP_ID"] as $group) {
        if ($group["GROUP_ID"] == USER_GROUP_ID_CONTENT_EDITORS) {
            Sender::noticeGroupOnAddUser();
        }
    }
}

function OnBeforeUserUpdate(&$arFields){
    $usersInGroup = CGroup::GetGroupUser(USER_GROUP_ID_CONTENT_EDITORS);
    if (!in_array($arFields["ID"], $usersInGroup)) {
        Sender::noticeGroupOnAddUser();
    }

}
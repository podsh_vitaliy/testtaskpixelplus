<?php

namespace Notice;

use Bitrix\Main\UserGroupTable;
use Bitrix\Main\UserTable;

class Sender
{
    public static function noticeGroupOnAddUser(array $noticeUsersID = array())
    {
        if(empty($noticeUsersID)){
            $filter = array();
            $filter['GROUP_ID'] = USER_GROUP_ID_CONTENT_EDITORS;

            $group = UserGroupTable::getList(array(
                'filter' => $filter,
                'select' => array('USER_ID')
            ))->fetchAll();

            foreach ($group as $user){
                $noticeUsersID[] = $user["USER_ID"];
            }
        }

        $filter = array('ID'=> $noticeUsersID);

        $users = UserTable::getList(array(
            'filter' => $filter,
            'select' => array('EMAIL')

        ))->fetchAll();

        $usersNotice = array();

        foreach ($users as $user){
            $usersNotice[] = $user["EMAIL"];
        }

        $emailTo = implode(', ' , $usersNotice);

        if (!empty($emailTo))
            \Bitrix\Main\Mail\Event::send(array(
                "EVENT_NAME" => "NEW_USER_CONTENT_GROUP",
                "LID" => "s1",
                'MESSAGE_ID' => '9',
                "C_FIELDS" => array('EMAIL_TO' => $emailTo)
            ));

    }
}
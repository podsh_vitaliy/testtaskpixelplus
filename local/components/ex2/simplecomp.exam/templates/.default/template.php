<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
?>

<ul>
<?foreach($arResult["ITEMS"] as $item) {?>
    <li>
        <?=$item["NAME"]. ' ' . $item["ACTIVE_FROM"] . ' (' . implode(', ',  $item["NAME_SECT_PROD"]) . ')' ;
        ?>
        <ul>
        <?foreach ($item["ELEMENTS"] as $el):?>
            <li>
                <?=$el["NAME"]?> - <?=intval($el["PROPERTY_PRICE_VALUE"])?> - <?=$el["PROPERTY_MATERIAL_VALUE"]?> - <?=$el["PROPERTY_ARTNUMBER_VALUE"]?>
            </li>
        <?endforeach;?>
        </ul>
    </li>
<? } ?>
</ul>



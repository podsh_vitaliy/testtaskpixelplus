<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentParameters = array(
    "PARAMETERS" => array(

        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("T_IBLOCK_DESC_ID"),
            "TYPE" => "STRING",
        ),
        "IBLOCK_ID_CLASIFICATOR" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("T_IBLOCK_DESC_ID_CLASIFICATOR"),
            "TYPE" => "STRING",
        ),
        "CODE_CLASIFICATOR" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("T_IBLOCK_DESC_CODE_CLASIFICATOR"),
            "TYPE" => "STRING",
        ),

        "CACHE_TIME"  =>  array("DEFAULT"=>36000000),
    ),
);

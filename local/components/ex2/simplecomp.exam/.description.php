<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage("T_IBLOCK_DESC_LIST"),
    "DESCRIPTION" => Loc::getMessage("T_IBLOCK_DESC_LIST_DESC"),
    "SORT" => 20,
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "NAME" => Loc::getMessage("T_IBLOCK_DESC_TITLE"),
            "SORT" => 10,
        ),
    ),
);

?>
<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class SimplecompExam extends CBitrixComponent
{
    function executeComponent()
    {
        Loc::loadMessages(__FILE__);

        if ($this->startResultCache()) {
            Loader::includeModule("iblock");


            $this->arResult['ITEMS'] = $this->getData();

            $this->setResultCacheKeys(array(
                "countElements"
            ));

            $this->includeComponentTemplate();
        }


        global $APPLICATION;
        $APPLICATION->SetTitle(Loc::getMessage("MESS_TITLE_COUNT",array("#count#"=> $this->arResult['countElements'])));

    }

    protected function getData(){
        $news = array();

        $sections = array();
        $filter = Array(
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "ACTIVE" => "Y",
            "!" . $this->arParams['CODE_CLASIFICATOR'] => false
        );

        $dbRes = CIBlockSection::GetList(Array(),
            $filter,
            false,
            array("ID", "NAME", $this->arParams["CODE_CLASIFICATOR"]));

        while ($res = $dbRes->GetNext()) {
            $sections[$res["ID"]] = $res;

            foreach ($res["UF_NEWS_LINK"] as $idNews)
                $news[$idNews]['NAME_SECT_PROD'][$res['ID']] = $res["NAME"];
        }

        $filter = Array(
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "ACTIVE" => "Y",
            'IBLOCK_SECTION_ID' => array_keys($sections)
        );
        $dbRes = CIBlockElement::GetList(Array(),
            $filter,
            false,
            false,
            array("ID", "NAME", 'IBLOCK_SECTION_ID',
                'PROPERTY_PRICE',
                'PROPERTY_MATERIAL',
                'PROPERTY_ARTNUMBER',
            ));

        while ($res = $dbRes->GetNext()) {
            $sections[$res["IBLOCK_SECTION_ID"]]['ELEMENTS'][] = $res;
        }

        $this->arResult['countElements'] = 0;
        $filter = Array(
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID_CLASIFICATOR"],
            "ACTIVE" => "Y",
            'ID' => array_keys($news)
        );

        $dbRes = \Bitrix\Iblock\ElementTable::getList(Array(
            'filter' => $filter,
            'select' => array("ID", "NAME", "ACTIVE_FROM")
        ));

        while ($res = $dbRes->fetch()) {
            $news[$res["ID"]] += $res;

            $addElements = array();

            foreach ($news[$res["ID"]]["NAME_SECT_PROD"] as $key => $item) {
                $addElements = array_merge($addElements, $sections[$key]['ELEMENTS']);

            }

            $news[$res["ID"]]['ELEMENTS'] = $addElements;
            $this->arResult['countElements'] += count($addElements);
        }

        return $news;
    }


}